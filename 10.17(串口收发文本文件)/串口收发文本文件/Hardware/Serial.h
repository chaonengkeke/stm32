#ifndef  __SERIAL_H
#define  __SERIAL_H

#include <stdio.h>

extern uint8_t Serial_RxFlag;
extern char Serial_RxPacket[];

void Serial_Init(void);
void Serial_SendByte(uint8_t Byte);
void Serial_SendArray(uint8_t *Array,uint16_t Lenth);
void Serial_SendString(char* String);
uint32_t  Serial_Pow(uint16_t x,uint16_t y);
void Serial_SendNumber(uint32_t Number,uint8_t Lenth);

#endif

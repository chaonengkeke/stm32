#include "stm32f10x.h"                  // Device header
#include "delay.h"

int main(void)
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.GPIO_Mode=GPIO_Mode_Out_PP;
	GPIO_InitStruct.GPIO_Pin=GPIO_Pin_0;
	GPIO_InitStruct.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init(GPIOA,&GPIO_InitStruct);
	
	GPIO_ResetBits(GPIOA,GPIO_Pin_0);
	GPIO_SetBits(GPIOA,GPIO_Pin_0);
	GPIO_WriteBit(GPIOA,GPIO_Pin_0,Bit_RESET);
	
	while(1)
	{
		GPIO_ResetBits(GPIOA,GPIO_Pin_0);//点亮LED
		Delay_ms(500);
		GPIO_SetBits(GPIOA,GPIO_Pin_0);//熄灭LED
		Delay_ms(500);
		GPIO_WriteBit(GPIOA,GPIO_Pin_0,(BitAction)0);//把1和0的类型强制转化为枚举类型
		Delay_ms(500);
		GPIO_WriteBit(GPIOA,GPIO_Pin_0,(BitAction)1);
		Delay_ms(500);
		
		GPIO_WriteBit(GPIOA,GPIO_Pin_0,Bit_RESET);//点亮LED
		Delay_ms(500);//在Delay.h函数里，如果需要使用，就必须配置文件和添加51新项目中
		GPIO_WriteBit(GPIOA,GPIO_Pin_0,Bit_SET);//熄灭LED
		Delay_ms(500);
	}
	
}

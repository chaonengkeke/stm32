#include "stm32f10x.h"                  // Device header
#include "Delay.h"
#include "OLED.h"
#include "Servo.h"
#include "Key.h"


uint8_t KeyNum;
float Angle;

int main(void)
{
	OLED_Init();
	Servo_Init();
	Key_Init();
	
	OLED_ShowString(1,1,"Angle:");
	
	Servo_SetAngle(90);//通过自定义函数设置角度
	//注：以下分别设置也可完成任务，但是比较麻烦不方便。
//	PWM_SetCompare2(500);//舵机为0°              示波器观察结果：f=50Hz，高电平时间=0.5ms
//	PWM_SetCompare2(2500);//舵机自动转为180°     示波器观察结果：f=50Hz，高电平时间=2.5ms
//	PWM_SetCompare2(1500);//舵机自动转为90°      示波器观察结果：f=50Hz，高电平时间=1.5ms
	
	while(1)
	{
		KeyNum=Key_GetNum();
		if(KeyNum==1)
		{
			Angle+=30;
			if(Angle>180)
			{
				Angle=0;
			}
		}Servo_SetAngle(Angle);
		OLED_ShowNum(1,7,Angle,3);
		
	}
}






#ifndef  __IC_H
#define  __IC_H


uint16_t IC_GetFreq(void);
void IC_Init(void);
uint16_t IC_GetDuty(void);

#endif

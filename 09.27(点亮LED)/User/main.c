#include "stm32f10x.h"                  // Device header



//操作STM需要3步：1.RCC开启时钟   2.GPIO_Init函数初始化GPIO  3.使用输出输入控函数控制GPIO口
		//需要RCC库函数+两个外设，(在最后几行)需要右键跳转库函数定义(RCC函数在Library里的stm32f10x...rcc.h)
		//RCC常用三个函数：RCC_AHB  RCC_APB2  RCC_APB1
		//AHB参数--@param:第一个参数选择哪个外设,第二个参数ENABLE or DISABLE
		//首先调用RCC里面的APB2外设时钟控制函数RCC_ClockSecuritySystemCmd
		
		
		//学习并使用gpio库函数:点击gpio.h并跳转定义并拖拽到最后查看参数类型(然后翻到最后面)
		//void GPIO_DeInit(GPIO_TypeDef* GPIOx);调用这个函数后，所指定的GPIO外设就会被复位
		//void GPIO_AFIODeInit(void);复位AFIO外设
		//void GPIO_Init 初始化外设：用结构体的参数初始化GPIO口，需要先定义结构体变量，再给结构体赋值，最后调用这个函数
		//void GPIO_StructInit 把结构体变量赋一个默认值
		//以下4个为GPIO读取函数：
		//uint8_t GPIO_ReadInputDataBit
		//uint16_t GPIO_ReadInputDatauint8_t 
		//GPIO_ReadOutputDataBit
		//uint16_t GPIO_ReadOutputData
		//以下4个为GPIO写入函数：
		//void GPIO_SetBits
		//void GPIO_ResetBits
		//void GPIO_WriteBit
		//void GPIO_Write
		
		
int main(void)
{
	
	//第一步：
	//首先调用RCC里面的APB2外设时钟控制函数RCC_ClockSecuritySystemCmd
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);
	//右键跳转到定义查找参数,需要点亮PA0的LED,所以选择第一个参数RCC_APB2Periph_GPIOA，第二个参数ENABLE开启时钟
	
	
	//第二步：
	GPIO_InitTypeDef GPIO_InitStructure;//函数名是地址
	//创建局部变量名字之后需要创建局部变量
	//右键跳转查找参数---在@ret后为参数,在ctrl+f搜索参数(因为注释部分不能直接搜索参数),电灯使用推挽输出,选择GPIO_Mode_Out_PP
	GPIO_InitStructure.GPIO_Mode=GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Pin=GPIO_Pin_0;//右键跳转有多个定义，选择member项，右键跳转定义，之后就可以找@ref后GPIO_pins_define为参数,ctrl+f找GPIO_Pin_0
	GPIO_InitStructure.GPIO_Speed=GPIO_Speed_50MHz;//右键GPIO_Speed,查找@ref,因为是注释，所以按ctrl+f,选择输出速度为GPIO_Speed_50MHz
	//最后将GPIO初始化地址给GPIO_Init的第二个参数
	GPIO_Init(GPIOA,&GPIO_InitStructure);//右键跳转到定义查看参数,第一个参数为GPIOA,第二个参数是结构体，需要复制结构体类型+名字
	
	//第三步：使用GPIO的输入输出函数  GPIO_SetBits(把指定端口设定为高电平)    GPIO_ResetBits(把指定端口设定为低电平)   GPIO_WriteBit   GPIO_Write   每个函数括号内为参数
	GPIO_ResetBits(GPIOA,GPIO_Pin_0);//查找参数点击该函数名称并找到@param后为参数,点亮
	GPIO_SetBits(GPIOA,GPIO_Pin_0);//熄灭
	GPIO_WriteBit(GPIOA,GPIO_Pin_0,Bit_RESET);//右键点击函数名，跳转查找@param,发现如下：@arg Bit_RESET: to clear the port pin清除端口值，置低电平---亮
	                                 //                                       @arg Bit_SET: to set the port pin    设置端口值，置高电平---灭
																	 
	
	
	
	while(1)
	{
		
		
		
		
	}
}

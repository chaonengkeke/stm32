#include "stm32f10x.h"                  // Device header
#include "Delay.h"
#include "OLED.h"



int main(void)
{
		OLED_Init();
		OLED_ShowChar(1, 1, 'A');//在第一行一列显示A，字符需要用''
		OLED_ShowString(1, 3, "HelloWorld!");//在第一行第三列开始显示HelloWorld!,
		                       //注：此处要查找数字的长度，超过就会截断，无法显示
		OLED_ShowNum(2, 1, 12345, 5);//在第二行一列显示12345，显示五个长度，
		//                       注：长度要适当
		OLED_ShowSignedNum(2, 7, -66, 2);//显示-66，第一位显示符号位
		OLED_ShowHexNum(3, 1, 0xAA55, 4);//显示AA55，十六进制
		OLED_ShowBinNum(4, 1, 0xAA55, 16);//显示2进制的AA55，即10101010 01010101
		OLED_Clear();//清屏。若只想清除某一位或某几位，使用OLED_ShowString();并添加空格.
	while(1)
	{
		
	}
}






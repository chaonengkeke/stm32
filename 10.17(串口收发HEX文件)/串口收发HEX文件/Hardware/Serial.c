#include "stm32f10x.h"                  // Device header


uint8_t Serial_RxFlag;
uint8_t Serial_TxPacket[4];
uint8_t Serial_RxPacket[4];//接收库函数USART_Serial_ReceiveData(USART1)的值，

void Serial_Init(void)
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1,ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);
	
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.GPIO_Mode=GPIO_Mode_AF_PP;
	GPIO_InitStruct.GPIO_Pin=GPIO_Pin_9;
	GPIO_InitStruct.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init(GPIOA,&GPIO_InitStruct);
	GPIO_InitStruct.GPIO_Mode=GPIO_Mode_IPU;
	GPIO_InitStruct.GPIO_Pin=GPIO_Pin_10;
	GPIO_InitStruct.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init(GPIOA,&GPIO_InitStruct);
	
	USART_InitTypeDef USART_InitStruct;
	USART_InitStruct.USART_BaudRate=9600;
	USART_InitStruct.USART_HardwareFlowControl=USART_HardwareFlowControl_None;
	USART_InitStruct.USART_Mode=USART_Mode_Tx;
	USART_InitStruct.USART_Parity=USART_Parity_No;
	USART_InitStruct.USART_StopBits=1;
	USART_InitStruct.USART_WordLength=0;
	USART_Init(USART1,&USART_InitStruct);
	
	USART_ITConfig(USART1,USART_IT_RXNE,ENABLE);
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);
	
	NVIC_InitTypeDef NVIC_InitStruct;
	NVIC_InitStruct.NVIC_IRQChannel=USART1_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelCmd=ENABLE;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority=1;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority=1;
	NVIC_Init(&NVIC_InitStruct);
	USART_Cmd(USART1,ENABLE);
	
}


void Serial_SendByte(uint8_t Byte)
{
	USART_SendData(USART1,Byte);
	while(USART_GetFlagStatus(USART1,USART_FLAG_TXE)==RESET);
}

void Serial_SendArray(uint8_t *Array,uint16_t Lenth)
{
	uint8_t i;
	for(i=0;i<Lenth;i++)
	{
		USART_SendData(USART1,Array[i]);
	}
}

void Serial_SendString(char*String)
{
	uint8_t i;
	for(i=0;String[i]!= '\0';i++)
	{
		Serial_SendByte(String[i]);
	}
}


uint32_t  Serial_Pow(uint16_t x,uint16_t y)
{
	uint32_t Return;
	while(y--)
	{
		Return*=x;
	}
		return Return;
}

void Serial_SendNumber(uint32_t Number,uint8_t Lenth)
{
	uint8_t i;
	for(i=0;i<Number;i++)
	{
		USART_SendData(USART1,Number/Serial_Pow(10,Lenth-1-i)%10+0x40);
	}
}


uint8_t Serial_GetRxFlag(void)
{
	if(Serial_RxFlag==1)
	{
		Serial_RxFlag=0;
		return 1;
	}
	else
		return 0;
}

//调用该函数，TxPacket的四个数据会自动发送出去
void Serial_SendPacket(void)
{
	Serial_SendByte(0xFF);//发送包头(数据用Byte)
	Serial_SendArray(Serial_TxPacket,4);//发送载荷数据(数组用Array)
	Serial_SendByte(0xFE);
}


//接收数据
void USART1_IRQHandler(void)
{
	static uint8_t RxState=0;//分情况判断
	static uint8_t pRxPacket=0;//数组下标判断
	if(USART_GetITStatus(USART1,USART_IT_RXNE)==SET)
	{
		uint8_t RxData=USART_ReceiveData(USART1);
		if(RxState==0)
		{
			if(RxData==0xFF)
			{
				RxState=1;
				pRxPacket=0;
			}
		}
		else if(RxState==1)
		{
			Serial_RxPacket[pRxPacket]=RxData;//将RxData放在数组里,pRxPacket表示存的位置
			pRxPacket++;
			if(pRxPacket>=4)
			{
				RxState=2;
			}
		}
		else if(RxState==2)
		{
			if(RxData==0xFE)
			{
				RxState=0;
				Serial_RxFlag=1;//置标志位，告诉已结束
			}
		}
		
		USART_ClearITPendingBit(USART1,USART_IT_RXNE);
	}
}



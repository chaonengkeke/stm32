#include "stm32f10x.h"                  // Device header
//起这个名字是为了防止于库函数的DMA(数据传输库函数)冲突


uint16_t MyDMA_Size;
//思路参考DMA基本结构图
//1.RCC开启DMA时钟
//2.初始化DMA_Init结构体
//3.开关控制DMA_Cmd
void MyDMA_Init(uint32_t AddrA,uint32_t AddrB,uint16_t Size)//传参,
//第一个参数：源端地址，第二个参数：目的地址，第三个参数：传输次数
{
	MyDMA_Size=Size;
	//1.RCC开启DMA时钟
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1,ENABLE);
	//2.初始化DMA_Init结构体
	DMA_InitTypeDef DMA_InitStructure;
	DMA_InitStructure.DMA_MemoryBaseAddr=AddrB;//存储器站点的起始地址(32位)，参数代替固定的地址。
	DMA_InitStructure.DMA_MemoryDataSize=DMA_MemoryDataSize_Byte;//存储器站点的数据宽度(字节形式)
	DMA_InitStructure.DMA_MemoryInc=DMA_MemoryInc_Enable;//存储器站点的是否自增
	DMA_InitStructure.DMA_PeripheralBaseAddr=AddrA;//外设站点的起始地址
	DMA_InitStructure.DMA_PeripheralDataSize=DMA_PeripheralDataSize_Byte;//外设站点的数据宽度
	DMA_InitStructure.DMA_PeripheralInc=DMA_PeripheralInc_Enable;//外设站点的是否自增
	DMA_InitStructure.DMA_BufferSize=Size;//缓冲区大小,(传输计数器，指定传输几次)
	DMA_InitStructure.DMA_DIR=DMA_DIR_PeripheralSRC;//数据传输方向(DST:存储器-->外设站点，SRC:外设站点-->存储器)
	DMA_InitStructure.DMA_M2M=DMA_M2M_Enable;//选择硬件触发还是软件触发(M2M_Enable存储器到存储器就是软件触发,Disable是硬件触发)
	DMA_InitStructure.DMA_Mode=DMA_Mode_Normal;//传输模式(指定是否自动重装)DMA_Mode_Normal：正常模式,转运之后停止
	DMA_InitStructure.DMA_Priority=DMA_Priority_Medium;//优先级(多个通道需要设置优先级,此处无所谓)
	DMA_Init(DMA1_Channel1,&DMA_InitStructure);//把结构体配置的参数给DMA1里的通道1.
	//第一个参数：既选择了是哪个DMA，又选择了是哪个通道。
	
	//3.开关控制DMA_Cmd
	DMA_Cmd(DMA1_Channel1,DISABLE);
	
}//转运一次，传输寄存器自减一次。传输寄存器为0时，转运结束。


//调用该函数，就在进行一次转运(给传输计数器重新赋值)
void MyDMA_Transfer(void)
{
	DMA_Cmd(DMA1_Channel1,DISABLE);//关闭
	DMA_SetCurrDataCounter(DMA1_Channel1,MyDMA_Size);//给传输计数器赋值
	DMA_Cmd(DMA1_Channel1,ENABLE);//开启
	while(DMA_GetFlagStatus(DMA1_FLAG_TC1)==RESET);//检查DMA1的通道1转运完成标志位,转运完成，标志位置1
	DMA_ClearFlag(DMA1_FLAG_TC1);//清除标志位状态
}



//补充的DMA(数据传输部分)库函数
//void DMA_ITConfig(DMA_Channel_TypeDef* DMAy_Channelx, uint32_t DMA_IT, FunctionalState NewState);//中断输出使能
//void DMA_SetCurrDataCounter(DMA_Channel_TypeDef* DMAy_Channelx, uint16_t DataNumber); //给传输计数器赋值
//uint16_t DMA_GetCurrDataCounter(DMA_Channel_TypeDef* DMAy_Channelx);//获取数据寄存器的数据
//FlagStatus DMA_GetFlagStatus(uint32_t DMAy_FLAG);//获取标志位状态函数
//void DMA_ClearFlag(uint32_t DMAy_FLAG);清除标志位状态
//ITStatus DMA_GetITStatus(uint32_t DMAy_IT);//获取中断状态
//void DMA_ClearITPendingBit(uint32_t DMAy_IT);//清除中断状态



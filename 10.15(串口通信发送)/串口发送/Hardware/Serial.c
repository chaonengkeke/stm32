#include "stm32f10x.h"                  // Device header
#include <stdio.h>

//思路：参考USART基本结构图
//1.开启时钟，SUART和GPIO
//2.GPIO初始化，配置TX为复用输出，RX为输入。
//3.配置USART(结构体)
//4.发送功能：开启USART..接收功能：配置中断(配置USART之前+ITConfig和NVIC的代码)。
//5.发送数据，就调用发送数据函数；接收数据，就调用接收数据函数。获取发送和接收状态，调用获取标志位的函数。
void Serial_Init(void)
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1,ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);
	
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Mode=GPIO_Mode_AF_PP;//TX是外设的输出脚，使用推挽输出
	GPIO_InitStructure.GPIO_Pin=GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init(GPIOA,&GPIO_InitStructure);
	
	USART_InitTypeDef USART_InitStruct;
	USART_InitStruct.USART_BaudRate=9600;//波特率(函数内部自己计算)
	USART_InitStruct.USART_HardwareFlowControl=USART_HardwareFlowControl_None;
	//USART_HardwareFlowControl是硬件流控制(None不使用流控;只使用CTS;只用RTS;CTS,RTS都使用)
	USART_InitStruct.USART_Mode=USART_Mode_Tx;//TX只有发送模式，RX只有接收模式(两个都需要就...RX|....TX)
	USART_InitStruct.USART_Parity=USART_Parity_No;//校验位(Odd奇校验,Even偶校验,No无校验)
	USART_InitStruct.USART_StopBits=USART_StopBits_1;//n位停止位(可选择0.5   1    1.5    2)
	USART_InitStruct.USART_WordLength=USART_WordLength_8b;//字长(不校验字长即为8位)
	USART_Init(USART1,&USART_InitStruct);
	
	USART_Cmd(USART1,ENABLE);
	
}


//发送数据函数，调用该函数，可以从TX引脚发送一个字节数据
void Serial_SendByte(uint8_t Byte)
{
	USART_SendData(USART1,Byte);
	while(USART_GetFlagStatus(USART1,USART_FLAG_TXE)==RESET);//等待TX置1
}


void Serial_SendArray(uint8_t *Array,uint16_t Length)//Array是数组，数组名是首元素地址,*Array是解引用
{
	uint16_t i;
	for(i=0;i<Length;i++)
	{
		Serial_SendByte(Array[i]);//依次取出数组Array的每一项并发送
	}
	
}

void Serial_SendString(char *String)//字符串自带结束标志位
{
	char i;
	for(i=0;String[i]!=0;i++)
	//此处0是字符0，是空字符，对应结束标志,如果不等于0就是继续循环，等于0就是结束，停止循环,等价于'\0'
	{
		Serial_SendByte(String[i]);
	}
	
}


uint32_t Serial_Pow(uint32_t X,uint32_t Y)//次方函数,返回值是X的Y次方
{
	uint32_t Result=1;
	while(Y--)
	{
		Result*=X;
	}
	return Result;
}


void Serial_SendNumber(uint32_t Number, uint8_t Length)//发送一个数字，电脑显示字符串形式的数字。
{
	uint8_t i;
	for (i = 0; i < Length; i ++)
	{
		Serial_SendByte(Number / Serial_Pow(10, Length - i - 1) % 10 + '0');
	}
}

int fputc(int ch,FILE*f)
{
	Serial_SendByte(ch);
	//将prinft输出的东西从屏幕改为输出到串口,需要重定向。----解释：printf也是使用fputc一个一个进行打印的
	return ch;
}




//void USART_SendData(USART_TypeDef* USARTx, uint16_t Data);//发送数据,写DR寄存器
//uint16_t USART_ReceiveData(USART_TypeDef* USARTx);//接收数据，读DR寄存器


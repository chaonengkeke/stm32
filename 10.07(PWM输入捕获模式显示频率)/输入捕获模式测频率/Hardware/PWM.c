#include "stm32f10x.h"                  // Device header


//操作思路可参考：(图PWM基本结构)
//1.RCC开启时钟，把TIM外设和GPIO外设的时钟打开。
//2.初始化时基单元，选择内部时钟-----使用的还是TIM2，所以直接复制粘贴内部时钟部分代码
//3.初始化输出比较单元，(重要函数TIM_OC1Init)包括CCR的值，输出比较模式，极性选择，输出使能。(结构体)
//4.初始化GPIO，(第三步的输出比较模式的值需要GPIO输出展示)TIM_OC1Init,GPIO口初始化为复用推挽输出
//5.运行控制，启动计数器。
//注：两个重要函数：
//用结构体初始化输出比较单元函数:TIM_OC1Init(在初始化PWM第三步)
//更改占空比，实现LED呼吸现象需要用的函数：PWM_SetCompare1(单独封装)

void PWM_Init(void)
{
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2,ENABLE);//第一步：开启APB1时钟,因为TIM2是APB1总线的外设
	
	TIM_InternalClockConfig(TIM2);//第二步：选择内部时钟
	//第二步：初始化时基单元----TIM_TimeBaseInit的第二个参数是结构体，配置结构体
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure;
	TIM_TimeBaseInitStructure.TIM_ClockDivision=TIM_CKD_DIV1;
	//TIM_CKD_DIV1,1分频，不分配；TIM_CKD_DIV2，2分频；TIM_CKD_DIV4，4分频----要求不高时，随意选择
	TIM_TimeBaseInitStructure.TIM_CounterMode=TIM_CounterMode_Up;
	//TIM_CounterMode_Up向上计数；TIM_CounterMode_Down向下计数；TIM_CounterMode_CenterAligned1等为中央对齐
	TIM_TimeBaseInitStructure.TIM_Period=100-1;//自动重装器的值，在10K频率下记10K个数，即1s一个。---ARR的值   
	TIM_TimeBaseInitStructure.TIM_Prescaler=720-1;//预分频，对72M进行7200分频----PSC的值
	TIM_TimeBaseInitStructure.TIM_RepetitionCounter=0;//高级定时器用到，通用定时器选0
	TIM_TimeBaseInit(TIM2,&TIM_TimeBaseInitStructure);//结构体
	
	//第三步：初始化输出比较单元,总共有四个，需要哪个通道(此处选PA0口,使用TIM_OC1Init)，初始化哪个函数
	TIM_OCInitTypeDef TIM_OCInitStructure;
	TIM_OCStructInit(&TIM_OCInitStructure);//给结构体赋初始值
	//TIM_OCInitStructure.TIM_OCIdleState=;//高级定时器才会用到
	TIM_OCInitStructure.TIM_OCMode=TIM_OCMode_PWM1;//设置输出比较模式,此处选择PWM模式1.
	//TIM_OCInitStructure.TIM_OCNIdleState=;//高级定时器
	//TIM_OCInitStructure.TIM_OCNPolarity=;//高级定时器才会用到
	TIM_OCInitStructure.TIM_OCPolarity=TIM_OCPolarity_High;//设置输出比较极性,TIM_OCPolarity_High有效电平为高电平
	//TIM_OCInitStructure.TIM_OutputNState=;//高级定时器才会用到
	TIM_OCInitStructure.TIM_OutputState=TIM_OutputState_Enable;//设置输出使能,TIM_OutputState_Enable使能
	TIM_OCInitStructure.TIM_Pulse=50;//设置CCR寄存器的值。占空比为50%---占空比越高，高电平占比越大，LED越亮
	TIM_OC1Init(TIM2, &TIM_OCInitStructure);//初始化OC1通道，输出波形,使之在GPIO口中输出波形
	
	//第四步：初始化GPIO
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Mode=GPIO_Mode_AF_PP;//选择复用推挽输出，使外设控制引脚(参照复用开漏/推挽输出图)
	GPIO_InitStructure.GPIO_Pin=GPIO_Pin_0;
	GPIO_InitStructure.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init(GPIOA,&GPIO_InitStructure);
	
	
	//第五步：启动定时器
	TIM_Cmd(TIM2,ENABLE);
	
}
//现象：有条件的可以买示波器观察PWM波形(矩形波，占空比50%)调节CCR,占空比越高，高电平占比越大，LED越亮



//单独更改占空比的值
//更改占空比需要用的PWM_SetCompare1
void PWM_SetCompare1(uint16_t Compare)
{
	TIM_SetCompare1(TIM2,Compare);
}



//初始化之后单独修改PSC---调用库函数专门修改PSC
void PWM_SetPrescaler(uint16_t Prescaler)
{
	TIM_PrescalerConfig(TIM2,Prescaler,TIM_PSCReloadMode_Immediate);
	//第二个参数：写入Prescaler的值。
	//第三个参数：重装模式：(写入的值的生效时间)
	//TIM_PSCReloadMode_Update：在更新时重装(更新时间生效)；
	//TIM_PSCReloadMode_Immediate：立即重装(立刻生效---可能会切断波形)。
}



//PWM调节频率
//PWM频率=更新频率=72M/(PSC+1)/(ARR+1),原理上PSC,ARR均可调节频率，但是占空比=CCR/(ARR+1),
//因此推荐控制变量PSC，固定ARR=(100-1)
//先确定分辨率如0.1%，分辨率=1000-1.就可知，PSC决定频率，频率=72M/预分配/1000，CCR决定占空比，占空比=CCR/1000
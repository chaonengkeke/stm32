#include "stm32f10x.h"                  // Device header
#include "Delay.h"
#include "OLED.h"
#include "Serial.h"


int main(void)
{
	OLED_Init();
	Serial_Init();
	//发送字节
	Serial_SendByte(0x41);//TX发送一个字节的数据0x41的波形，该波形可以传递给电脑等其他接收端。
	//发送数组
	Serial_SendByte('A');//也可以字符的形式发送,此时先对数据进行编码，然后变为0x41
	//发送数组
	uint8_t MyArray[]={0x42,0x43,0x44,0x45};
	Serial_SendArray(MyArray,4);//一般使用在HEX模式
	//发送字符串
	Serial_SendString("HelloWord!");//一般使用在文本模式
	//发送字符形式的数字
	Serial_SendNumber(111, 3);
	//使用printf打印
	printf("Num=%d\r\n",666);
	
	char String[100];//定义字符串
	sprintf(String,"Num=%d\r\n", 333);//sprintf可定义打印字符串的位置
	Serial_SendString(String);//发送字符串String
	
	
	while(1)
	{
		
	}
}





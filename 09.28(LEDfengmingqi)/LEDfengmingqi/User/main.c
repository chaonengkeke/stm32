#include "stm32f10x.h"                  // Device header
#include "delay.h"

int main(void)
{
	
	//初始化PB12口
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB,ENABLE);//时钟，用PB口
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.GPIO_Mode=GPIO_Mode_Out_PP;//推挽输出
	GPIO_InitStruct.GPIO_Pin=GPIO_Pin_12;//PB12
	GPIO_InitStruct.GPIO_Speed=GPIO_Speed_50MHz;//速度50MHz
	GPIO_Init(GPIOB,&GPIO_InitStruct);//对GPIOB的初始化
	
	
	while(1)
	{
		GPIO_ResetBits(GPIOB,GPIO_Pin_12);
		Delay_ms(500);
		GPIO_SetBits(GPIOB,GPIO_Pin_12);
		Delay_ms(500);//调整延时函数参数500的大小，可改变声音的长短

	}
	
}

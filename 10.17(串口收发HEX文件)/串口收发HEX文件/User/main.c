#include "stm32f10x.h"                  // Device header
#include "Delay.h"
#include "OLED.h"
#include "Serial.h"
#include "Key.h"

uint8_t KeyNum;

int main(void)
{
	OLED_Init();
	Serial_Init();
	OLED_ShowString(1,1,"RxData");
	OLED_ShowString(3,1,"TxData");
	
	//赋初值
	Serial_TxPacket[0]=0x01;
	Serial_TxPacket[1]=0x02;
	Serial_TxPacket[2]=0x03;
	Serial_TxPacket[3]=0x04;
	
	
	while (1)
	{
		KeyNum = Key_GetNum();
		if(KeyNum==1)
		{
			Serial_SendPacket();//发送数据包
			
			Serial_TxPacket[0]++;
			Serial_TxPacket[1]++;
			Serial_TxPacket[2]++;
		  Serial_TxPacket[3]++;
			
			OLED_ShowHexNum(2,1,Serial_TxPacket[0],2);
			OLED_ShowHexNum(2,4,Serial_TxPacket[1],2);
			OLED_ShowHexNum(2,7,Serial_TxPacket[2],2);
			OLED_ShowHexNum(2,10,Serial_TxPacket[3],2);
			
		}
		if(Serial_GetRxFlag() == 1)//标志位结束判断
		{
			OLED_ShowHexNum(4,1,Serial_TxPacket[0],2);
			OLED_ShowHexNum(4,4,Serial_TxPacket[1],2);
			OLED_ShowHexNum(4,7,Serial_TxPacket[2],2);
			OLED_ShowHexNum(4,10,Serial_TxPacket[3],2);
		}
		
	}
}

#include "stm32f10x.h"                  // Device header
#include "delay.h"

int main(void)
{
	//GPIO三步骤：(RCC+GPIO两个外设)---右键跳转定义.h文件，然后拉到最后
		//1.RCC开启GPIO时钟
		//2.GPIO_Init初始化GPIO，
		//3.使用输出，输入控制GPIO口
	
	//1.RCC初始化函数
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);
	//2.GPIO_Init初始化GPIO
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.GPIO_Mode=GPIO_Mode_Out_PP;
	GPIO_InitStruct.GPIO_Pin=GPIO_Pin_All;//流水灯需要0~7号端口
	GPIO_InitStruct.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init(GPIOA,&GPIO_InitStruct);
	

	while(1)
	{
		
		//GPIO控制流水灯
		//GPIO需要同时控制16个引脚----只用第八位，高八位不需要
		GPIO_Write(GPIOA,~0x0001);
		//不支持2进制，此处是16进制。0000 0000 0000 0001,低电平点亮，按位取反.第一个LED点亮，其他都熄灭
		Delay_ms(500);
		GPIO_Write(GPIOA,~0x0002);
		//不支持2进制，此处是16进制。0000 0000 0000 0002,低电平点亮，按位取反.第一个LED点亮，其他都熄灭
		Delay_ms(500);
		GPIO_Write(GPIOA,~0x0004);
		//不支持2进制，此处是16进制。0000 0000 0000 0004,低电平点亮，按位取反.第一个LED点亮，其他都熄灭
		Delay_ms(500);
		GPIO_Write(GPIOA,~0x0008);
		//不支持2进制，此处是16进制。0000 0000 0000 0008,低电平点亮，按位取反.第一个LED点亮，其他都熄灭
		Delay_ms(500);
		GPIO_Write(GPIOA,~0x0010);
		//不支持2进制，此处是16进制。0000 0000 0000 0010,低电平点亮，按位取反.第一个LED点亮，其他都熄灭
		Delay_ms(500);
		GPIO_Write(GPIOA,~0x0020);
		//不支持2进制，此处是16进制。0000 0000 0000 0020,低电平点亮，按位取反.第一个LED点亮，其他都熄灭
		Delay_ms(500);
		GPIO_Write(GPIOA,~0x0040);
		//不支持2进制，此处是16进制。0000 0000 0000 0040,低电平点亮，按位取反.第一个LED点亮，其他都熄灭
		Delay_ms(500);
		GPIO_Write(GPIOA,~0x0080);
		//不支持2进制，此处是16进制。0000 0000 0000 0080,低电平点亮，按位取反.第一个LED点亮，其他都熄灭
		Delay_ms(500);
		
		
		
		
		//GPIO_Mode_Out_PP，推挽输出下，高低电平都有驱动
		//GPIO_Mode_Out_OD，开漏输出模式下，高电平(相当于高阻态)没有驱动能力,低电平有
		
		
	}
}

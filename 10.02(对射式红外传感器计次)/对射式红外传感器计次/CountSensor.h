#ifndef  __COUNT_SENSOR_H
#define  __COUNT_SENSOR_H

void CountSensor_Init(void);
//void EXTI15_10_IRQHandler(void);//注：中断函数不需要声明，会自动调用的
uint16_t CountSensor_Count_Get(void);

#endif

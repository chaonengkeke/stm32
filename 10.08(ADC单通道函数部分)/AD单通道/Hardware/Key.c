#include "stm32f10x.h"                  // Device header
#include "Delay.h"


void Key_Init(void)//初始化按键
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB,ENABLE);//按键连接在GPIOB上，使能管脚
	
	GPIO_InitTypeDef GPIO_InitStucture;
	GPIO_InitStucture.GPIO_Mode=GPIO_Mode_IPU;//读取按键需要上拉输入，GPIO_Mode_IPU
	GPIO_InitStucture.GPIO_Pin=GPIO_Pin_1|GPIO_Pin_11;//接在GPIO1和GPIO11
	GPIO_InitStucture.GPIO_Speed=GPIO_Speed_50MHz;//GPIO的输出速度(输入速度没用)
	GPIO_Init(GPIOB,&GPIO_InitStucture);
	
}


uint8_t Key_GetNum(void)//独立按键---1管脚的控制---按下点亮，再按，熄灭
{
	uint8_t Key_Num=0;
	if(GPIO_ReadInputDataBit(GPIOB,GPIO_Pin_1)==0)
	{
		Delay_ms(20);
		while(GPIO_ReadInputDataBit(GPIOB,GPIO_Pin_1)==0);
		Delay_ms(20);
		Key_Num=1;
	}
	
	if(GPIO_ReadInputDataBit(GPIOB,GPIO_Pin_11)==0)//独立按键---11管脚的控制---按下点亮，再按，熄灭
	{
		Delay_ms(20);
		while(GPIO_ReadInputDataBit(GPIOB,GPIO_Pin_11)==0);
		Delay_ms(20);
		Key_Num=2;
	}
	
	return Key_Num;
}




//注：
//GPIO_ReadInputDataBit---读取输入数据寄存器的某一位(第二个参数)
//GPIO_ReadInputData------读取整个输入数据寄存器
//GPIO_ReadOutputDataBit--读取输入数据寄存器的某一位
//GPIO_ReadOutputData------读取整个输出数据寄存器
	
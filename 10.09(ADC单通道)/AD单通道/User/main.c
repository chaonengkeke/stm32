#include "stm32f10x.h"                  // Device header
#include "Delay.h"
#include "OLED.h"
#include "AD.h"

uint16_t ADValue;
float Voltage;

int main(void)
{
	OLED_Init();
	AD_Init();
	
	OLED_ShowString(1,1,"ADValue");//显示AD值(0~4095)
	OLED_ShowString(2,1,"Voltage");//显示电压值(0~3.3V)

	while(1)
	{
		ADValue=AD_GETValue();//启动等待读取
		Voltage=(float)ADValue/4095*3.3;//将ADValue的0~4095变为0~3.3V
		OLED_ShowNum(1,9,ADValue,4);
		OLED_ShowNum(2,9,Voltage,1);//只会显示整数的部分，因为该OLED没小数点,需要自行计算小数点部分
    OLED_ShowNum(2,11,(uint16_t)(Voltage*100)%100,2);//浮点数不能取余，需要强制类型转换
		Delay_ms(100);//刷新/持续时间
	}
}

//现象：OLED上有AD值，并且左拧数据减小，右拧数据增大。
//注：AD的个位可能会持续跳动，属于正常现象，如果在固定场合规定不能跳动，如AD高于某一值就亮灯，低于就灭灯，
//就会导致频繁的亮灭，因此，可以：1.设置阈值，超过某一值灯就亮，低于某一值灯就灭。2.滤波。3.裁剪分辨率






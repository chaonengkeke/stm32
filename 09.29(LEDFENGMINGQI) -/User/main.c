#include "stm32f10x.h"                  // Device header
#include "delay.h"

int main(void)
{
	//GPIO三步骤：(RCC+GPIO两个外设)---右键跳转定义.h文件，然后拉到最后
		//1.RCC开启GPIO时钟
		//2.GPIO_Init初始化GPIO，
		//3.使用输出，输入控制GPIO口
	
	//1.RCC初始化函数---蜂鸣器使用的是PB口，改为GPIOB
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB,ENABLE);
	//2.GPIO_Init初始化GPIO
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.GPIO_Mode=GPIO_Mode_Out_PP;
	GPIO_InitStruct.GPIO_Pin=GPIO_Pin_12;//12号端口
	GPIO_InitStruct.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init(GPIOA,&GPIO_InitStruct);
	

	while(1)
	{
		
		//3.GPIO控制LED闪烁
		//LED闪烁模式1：
		GPIO_ResetBits(GPIOB,GPIO_Pin_12);//响
		Delay_ms(500);//改变时间可改变响或不响的时间，即频率
		GPIO_SetBits(GPIOB,GPIO_Pin_12);//不响
		Delay_ms(500);
	
		
		
	}
}

#include "stm32f10x.h"                  // Device header


//参考图PWM基本结构
//1.RCC开启时钟，把TIM外设和GPIO外设的时钟打开。
//2.配置时基单元，时钟源选择
//3.输出比较单元，包括CCR的值，输出比较模式，极性选择，输出使能。(结构体)
//4.配置GPIO，GPIO口初始化为复用推挽输出
//5.运行控制，启动计数器。
//现象：有条件的可以买示波器观察PWM波形(矩形波，占空比50%)调节CCR,占空比越高，高电平占比越大，LED越亮
void PWM_Init(void)
{
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2,ENABLE);//第一步：开启APB1时钟,因为TIM2是APB1总线的外设
	
	TIM_InternalClockConfig(TIM2);//第二步：选择时基单元的时钟(此处选的是内部时钟)
	
	
	
	//第三步：配置时基单元----TIM_TimeBaseInit的第二个参数是结构体，配置结构体
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure;
	TIM_TimeBaseInitStructure.TIM_ClockDivision=TIM_CKD_DIV1;
	//TIM_CKD_DIV1,1分频，不分配；TIM_CKD_DIV2，2分频；TIM_CKD_DIV4，4分频----要求不高时，随意选择
	TIM_TimeBaseInitStructure.TIM_CounterMode=TIM_CounterMode_Up;
	//TIM_CounterMode_Up向上计数；TIM_CounterMode_Down向下计数；TIM_CounterMode_CenterAligned1等为中央对齐
	TIM_TimeBaseInitStructure.TIM_Period=20000-1;//自动重装器的值，在10K频率下记10K个数，即1s一个。---ARR的值   
	TIM_TimeBaseInitStructure.TIM_Prescaler=72-1;//预分频，对72M进行7200分频----PSC的值
	TIM_TimeBaseInitStructure.TIM_RepetitionCounter=0;//高级定时器用到，通用定时器选0
	TIM_TimeBaseInit(TIM2,&TIM_TimeBaseInitStructure);//结构体
	
	//初始化输出比较单元,总共有四个，需要哪个通道(GPIO口)，初始化哪个函数
	TIM_OCInitTypeDef TIM_OCInitStructure;
	TIM_OCStructInit(&TIM_OCInitStructure);//给结构体赋初始值
	//TIM_OCInitStructure.TIM_OCIdleState=;//高级定时器才会用到
	TIM_OCInitStructure.TIM_OCMode=TIM_OCMode_PWM1;//设置输出比较模式
	//TIM_OCInitStructure.TIM_OCNIdleState=;//高级定时器
	//TIM_OCInitStructure.TIM_OCNPolarity=;//高级定时器才会用到
	TIM_OCInitStructure.TIM_OCPolarity=TIM_OCPolarity_High;//设置输出比较极性,TIM_OCPolarity_High有效电平为高电平
	//TIM_OCInitStructure.TIM_OutputNState=;//高级定时器才会用到
	TIM_OCInitStructure.TIM_OutputState=TIM_OutputState_Enable;//设置输出使能,TIM_OutputState_Enable使能
	TIM_OCInitStructure.TIM_Pulse=50;//设置CCR寄存器的值。占空比为50%---占空比越高，高电平占比越大
	//TIM2的引脚复用在PA0上，在引脚说明手册里，默认复用功能对应的引脚不能更改，
	//但是，如果两个外设接在同一引脚，可查找重定义功能更改外设的引脚。
	//如需要用TIM的CH1通道输出PWM.则只能在PA0引脚输出，或在重映射改为PA15,因此需要初始化GPIO的PA0
	
	//初始化输出PWM的GPAO(舵机使用GPIO的PA1引脚)
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);
	
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Mode=GPIO_Mode_AF_PP;//选择复用推挽输出，使外设控制引脚(参照复用开漏/推挽输出图)
	GPIO_InitStructure.GPIO_Pin=GPIO_Pin_1;//使用PA1引脚，改为GPIO_Pin_1
	GPIO_InitStructure.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init(GPIOA,&GPIO_InitStructure);
	TIM_OC2Init(TIM2, &TIM_OCInitStructure);//此处使用PWM输出第二通道。
	
	
	//启动定时器
	TIM_Cmd(TIM2,ENABLE);
	
	
	//ARR,PSC,CCR是用来计算频率，占空比和分辨率的(参考参数计算)
	//计算时，频率，占空比，分辨率是自己设置的，是已知的，因此，变为解方程组，
	//(频率为1/20ms=50Hz，占空比50%,分辨率1%)
	//PSC+1=72,ARR+1=20K;CCR=500~2500--->CCR=0.5ms~2.5ms
	
}


//舵机使用2通道设置CCR
void PWM_SetCompare2(uint16_t Compare)
{
	TIM_SetCompare1(TIM2,Compare);
}


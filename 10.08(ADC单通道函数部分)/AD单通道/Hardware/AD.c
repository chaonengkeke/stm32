#include "stm32f10x.h"                  // Device header


//思路如下：(参照ADC基本结构图)
//1.开启RCC时钟，包括ADC和GPIO的时钟，配置ADCCLOCK的分频器
//2.配置GPIO为模拟输入模式
//3.配置多路开关，把左边的通道(如GPIO)接到右边的规则组里
//4.配置ADC转换器，用结构体
//(参数决定ADC是单次转换还是连续转换，扫描还是非扫描，有几个通道，触发源是什么，数据对齐是左对齐还是右对齐)
//5.调用ADC_Cmd进行开关控制
void AD_Init(void)
{
	//1.开启RCC时钟
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1,ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);
	RCC_ADCCLKConfig(RCC_PCLK2_Div6);//对APB2进行6分频
	//2.配置GPIO为模拟输入模式(可复制LED.c初始化部分代码)
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Mode=GPIO_Mode_AIN;//AIN是ADC专属模式(模拟输入模式下，GPIO无效，防止对ADC产生干扰)
	GPIO_InitStructure.GPIO_Pin=GPIO_Pin_0;//PA0口初始化为模拟输入的引脚
	GPIO_InitStructure.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init(GPIOA,&GPIO_InitStructure);
	//3.选择输入通道
	ADC_RegularChannelConfig(ADC1,ADC_Channel_0, 1,ADC_SampleTime_55Cycles5);//在规则组列表的第一个位置写入通道0
	//参数二：选择通道0，   参数三：序列1~16之间的一个数    参数四：指定通道的采样时间(转换快选择参数,稳定选大参数)
	
	//4.用结构体初始化ADC
	
}



//ADCCLK相关配置函数(在rcc.h里)：
//void RCC_ADCCLKConfig(uint32_t RCC_PCLK2);//配置ADCCLK分频器，对APB2的72MHz时钟选择2，4，6，8分频，输入到ADCCLK
//ADC相关库函数(在adc.h里)：
//void ADC_DeInit(ADC_TypeDef* ADCx);//恢复出厂配置
//void ADC_Init(ADC_TypeDef* ADCx, ADC_InitTypeDef* ADC_InitStruct);//初始化
//void ADC_StructInit(ADC_InitTypeDef* ADC_InitStruct);//配置结构体
//void ADC_Cmd(ADC_TypeDef* ADCx, FunctionalState NewState);//给ADC上电，即最后一步的开关控制
//void ADC_DMACmd(ADC_TypeDef* ADCx, FunctionalState NewState);//开启DMA输出信号
//void ADC_ITConfig(ADC_TypeDef* ADCx, uint16_t ADC_IT, FunctionalState NewState);//中断输出控制
//以下为控制校准相关函数：
//void ADC_ResetCalibration(ADC_TypeDef* ADCx);//复位校准
//FlagStatus ADC_GetResetCalibrationStatus(ADC_TypeDef* ADCx);//获取复位校准状态
//void ADC_StartCalibration(ADC_TypeDef* ADCx);//开始校准
//FlagStatus ADC_GetCalibrationStatus(ADC_TypeDef* ADCx);//获取开始校准状态

//void ADC_SoftwareStartConvCmd(ADC_TypeDef* ADCx, FunctionalState NewState);//ADC软件开始转换控制--触发控制的软件触发
//FlagStatus ADC_GetSoftwareStartInjectedConvCmdStatus(ADC_TypeDef* ADCx);//获取标志位状态，转换结束EOC置1
//以下为配置间断模式相关函数：
//void ADC_DiscModeChannelCountConfig(ADC_TypeDef* ADCx, uint8_t Number);//每个几个通道间断一次
//void ADC_DiscModeCmd(ADC_TypeDef* ADCx, FunctionalState NewState);//是否启用间断模式

//void ADC_RegularChannelConfig(ADC_TypeDef* ADCx, uint8_t ADC_Channel, uint8_t Rank, uint8_t ADC_SampleTime);
//ADC规则组通道配置，给序列的每个位置填写指定通道(重点)
//参数二：ADC_Channel为指定的通道；   参数三：ADC_Channel序列几；   参数四：ADC_SampleTime指定通道的采样时间

//void ADC_ExternalTrigConvCmd(ADC_TypeDef* ADCx, FunctionalState NewState);//判断是否允许ADC外部触发转换

//uint16_t ADC_GetConversionValue(ADC_TypeDef* ADCx);//获取AD转换的数据寄存器，读取结果时使用该函数。(重点)
//uint32_t ADC_GetDualModeConversionValue(void);//获取双模式转换结果

//FlagStatus ADC_GetFlagStatus(ADC_TypeDef* ADCx, uint8_t ADC_FLAG);//获取标志位状态
//void ADC_ClearFlag(ADC_TypeDef* ADCx, uint8_t ADC_FLAG);//清楚标志位
//ITStatus ADC_GetITStatus(ADC_TypeDef* ADCx, uint16_t ADC_IT);//获取中断状态
//void ADC_ClearITPendingBit(ADC_TypeDef* ADCx, uint16_t ADC_IT);//清除中断挂起位











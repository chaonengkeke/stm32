#include "stm32f10x.h"                  // Device header
#include "Delay.h"
#include "OLED.h"
#include "Timer.h"

uint16_t Num;

int main(void)
{
	OLED_Init();
	Timer_Init();
	

	
	OLED_ShowString(1, 1, "Num:");//在第一行第三列开始显示HelloWorld!,
	while(1)
	{
		OLED_ShowNum(1,5,Num,5);
	}
}



//中断函数：当进入中断，自动执行该函数
void TIM2_IRQHandler(void)
{
	if(TIM_GetITStatus(TIM2,TIM_IT_Update)==SET)//获取中断标志位
	{
		Num++;
		TIM_ClearITPendingBit(TIM2,TIM_IT_Update);//清除标志位
	}
	
}




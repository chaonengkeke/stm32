#include "stm32f10x.h"                  // Device header

void LED_Init(void)//LED初始化函数
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);
	
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Mode=GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Pin=GPIO_Pin_1|GPIO_Pin_2;
	GPIO_InitStructure.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init(GPIOA,&GPIO_InitStructure);
	
	GPIO_SetBits(GPIOA,GPIO_Pin_1|GPIO_Pin_2);//此行代码是使默认变为高电平，灯不亮的情况
	
}

void LED1_ON(void)//打开LED1
{
	GPIO_SetBits(GPIOA,GPIO_Pin_1);
}

void LED1_OFF(void)//关闭LED1
{
	GPIO_ResetBits(GPIOA,GPIO_Pin_1);
}

void LED2_ON(void)//打开LED2
{
	GPIO_SetBits(GPIOA,GPIO_Pin_2);
}

void LED2_OFF(void)//关闭LED2
{
	GPIO_ResetBits(GPIOA,GPIO_Pin_2);
}

void LED1_Turn(void)//反转GPIO按键，按下LED1熄灭，再按点亮
{
	if(GPIO_ReadOutputDataBit(GPIOA,GPIO_Pin_1)==0)//如果GPIO输出0，
	{
		GPIO_SetBits(GPIOA,GPIO_Pin_1);//则使用SetBits使之变为1
	}
	else//如果GPIO输出1，
	{
		GPIO_SetBits(GPIOA,GPIO_Pin_1);//则使用SetBits使之变为0
	}
}

void LED2_Turn(void)//反转GPIO按键，按下LED2熄灭，再按点亮
{
	if(GPIO_ReadOutputDataBit(GPIOA,GPIO_Pin_2)==0)//如果GPIO输出0，
	{
		GPIO_SetBits(GPIOA,GPIO_Pin_2);//则使用SetBits使之变为1
	}
	else//如果GPIO输出1，
	{
		GPIO_SetBits(GPIOA,GPIO_Pin_2);//则使用SetBits使之变为0
	}
}

#include "stm32f10x.h"                  // Device header
#include "Delay.h"
#include "OLED.h"
#include "PWM.h"

//现象：有条件的可以买示波器观察PWM波形(矩形波，占空比50%)调节CCR,占空比越高，高电平占比越大，LED越亮

uint8_t i;

int main(void)
{
	OLED_Init();
	PWM_Init();
	//如果没有下面while循环的进一步操作，则LED为常亮(实际是1KHz闪烁)
	while(1)
	{
		for(i=0;i<=100;i++)
		{
			PWM_SetCompare1(i);//完成占空比从0%~100%，亮灭逐渐变亮，达到呼吸灯效果
			Delay_ms(10);
		}
		
		for(i=0;i<=100;i++)
		{
			PWM_SetCompare1(100-i);//完成占空比从0%~100%，亮灭逐渐变暗，达到呼吸灯效果
			Delay_ms(10);
		}
	}
}






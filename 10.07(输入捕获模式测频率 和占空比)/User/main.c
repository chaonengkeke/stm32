#include "stm32f10x.h"                  // Device header
#include "Delay.h"
#include "OLED.h"
#include "PWM.h"
#include "IC.h"


int main(void)
{
	OLED_Init();
	PWM_Init();
	IC_Init();
	
	OLED_ShowString(1,1,"Freq:00000Hz");
	OLED_ShowString(2,1,"Duty:00%");
	//执行逻辑：PWM模块将待测信号输出给PA0,PA0通过导线输入到PA6,PA6是TIM3的通道1，
	//通道1通过输入捕获模块，测量得到频率，进入主循环，不断刷新频率
	PWM_SetPrescaler(720-1);//频率Freq=72M/(PSC+1)/(ARR+1),此时ARR+1=100,所以频率=1KHz
	PWM_SetCompare1(50);//占空比：Duty=CCR/(ARR+1),此时ARR+1=100，所以占空比=50%
	
	while(1)
	{
		OLED_ShowNum(1,6,IC_GetFreq(),5);
		OLED_ShowNum(2,6,IC_GetDuty(),2);
	}
}






#include "stm32f10x.h"                  // Device header
#include "PWM.h"


void Servo_Init(void)
{
	PWM_Init();
}


void Servo_SetAngle(float Angle)//舵机设置角度
{
	PWM_SetCompare2(Angle/180*2000+500);//输入值/180是占用总角度的比例，然后*2000是总比例+500是起始偏转角
	
}

#include "stm32f10x.h"                  // Device header
#include "delay.h"

int main(void)
{
	//GPIO三步骤：(RCC+GPIO两个外设)---右键跳转定义.h文件，然后拉到最后
		//1.RCC开启GPIO时钟
		//2.GPIO_Init初始化GPIO，
		//3.使用输出，输入控制GPIO口
	
	//1.RCC初始化函数    蜂鸣器使用的是PB口，改为GPIOB
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);
	//2.GPIO_Init初始化GPIO
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.GPIO_Mode=GPIO_Mode_Out_PP;
	GPIO_InitStruct.GPIO_Pin=GPIO_Pin_0;//0号端口
	GPIO_InitStruct.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init(GPIOA,&GPIO_InitStruct);
	

	while(1)
	{
		
		//3.GPIO控制LED闪烁
		//LED闪烁模式1：
		GPIO_WriteBit(GPIOA,GPIO_Pin_0,Bit_RESET);//点亮
		Delay_ms(500);//控制延时时间可改变闪烁间隙
		GPIO_WriteBit(GPIOA,GPIO_Pin_0,Bit_SET);//熄灭
		Delay_ms(500);
		
		//LED闪烁模式2：
		GPIO_ResetBits(GPIOA,GPIO_Pin_0);//把指定端口设定为低电平---点亮LED
		Delay_ms(500);
		GPIO_SetBits(GPIOA,GPIO_Pin_0);//把指定端口设定为高电平---熄灭LED
		Delay_ms(500);
		
		//LED闪烁模式3:枚举类型
		GPIO_WriteBit(GPIOA,GPIO_Pin_0,(BitAction)0);//通过第三个参数的值来指定端口
		//把1和0的类型强制转化为枚举类型
		Delay_ms(500);
		GPIO_WriteBit(GPIOA,GPIO_Pin_0,(BitAction)1);
		Delay_ms(500);
		
		//不推荐使用PA12  PB3 PB4这三个端口，因为这三个默认为JTAD调试端口，如果想作为普通端口，需要配置
		
		
	}
}
